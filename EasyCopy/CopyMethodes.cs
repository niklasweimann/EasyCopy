﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace EasyCopy
{
    class CopyMethodes
    {
        public bool BruteforceCopy(string From, string To)
        {
            
            try
            {
                foreach (var file in Directory.GetFiles(From))
                {
                    var oldFile = new FileInfo(Path.Combine(From + @"\", Path.GetFileName(file)));
                    var newFile = new FileInfo(Path.Combine(To, Path.GetFileName(file)));
                    if (newFile.Exists && oldFile.Exists)
                    {
                        if (oldFile.LastWriteTime > newFile.LastWriteTime)
                        {
                            File.Copy(Path.Combine(From + @"\", Path.GetFileName(file)),
                                Path.Combine(To, Path.GetFileName(file)),
                                true);
                        }
                    }
                    else
                    {
                        File.Copy(Path.Combine(From + @"\", Path.GetFileName(file)),
                                Path.Combine(To, Path.GetFileName(file)),
                                true);
                    }
                }
                return true;
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message.ToString());
                return false;
            }
        }


    }
}
