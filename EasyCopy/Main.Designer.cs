﻿namespace EasyCopy
{
    partial class Main
    {
        /// <summary>
        /// Erforderliche Designervariable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Verwendete Ressourcen bereinigen.
        /// </summary>
        /// <param name="disposing">True, wenn verwaltete Ressourcen gelöscht werden sollen; andernfalls False.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Vom Windows Form-Designer generierter Code

        /// <summary>
        /// Erforderliche Methode für die Designerunterstützung.
        /// Der Inhalt der Methode darf nicht mit dem Code-Editor geändert werden.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.dateiToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.beendenToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.hilfeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.überDiesesToolToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.FromTextBox = new System.Windows.Forms.TextBox();
            this.ToTextBox = new System.Windows.Forms.TextBox();
            this.FromButton = new System.Windows.Forms.Button();
            this.ToButton = new System.Windows.Forms.Button();
            this.TimeRadioButton = new System.Windows.Forms.RadioButton();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.ManuellRadioButton = new System.Windows.Forms.RadioButton();
            this.CopyButton = new System.Windows.Forms.Button();
            this.OnChangeRadioButton = new System.Windows.Forms.RadioButton();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.toolStripStatusLabel1 = new System.Windows.Forms.ToolStripStatusLabel();
            this.ModusToolStripStatusLabel = new System.Windows.Forms.ToolStripStatusLabel();
            this.toolStripStatusLabel3 = new System.Windows.Forms.ToolStripStatusLabel();
            this.toolStripStatusLabel4 = new System.Windows.Forms.ToolStripStatusLabel();
            this.menuStrip1.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.statusStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.dateiToolStripMenuItem,
            this.hilfeToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(591, 24);
            this.menuStrip1.TabIndex = 0;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // dateiToolStripMenuItem
            // 
            this.dateiToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.beendenToolStripMenuItem});
            this.dateiToolStripMenuItem.Name = "dateiToolStripMenuItem";
            this.dateiToolStripMenuItem.Size = new System.Drawing.Size(46, 20);
            this.dateiToolStripMenuItem.Text = "Datei";
            // 
            // beendenToolStripMenuItem
            // 
            this.beendenToolStripMenuItem.Name = "beendenToolStripMenuItem";
            this.beendenToolStripMenuItem.Size = new System.Drawing.Size(120, 22);
            this.beendenToolStripMenuItem.Text = "Beenden";
            this.beendenToolStripMenuItem.Click += new System.EventHandler(this.beendenToolStripMenuItem_Click);
            // 
            // hilfeToolStripMenuItem
            // 
            this.hilfeToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.überDiesesToolToolStripMenuItem});
            this.hilfeToolStripMenuItem.Name = "hilfeToolStripMenuItem";
            this.hilfeToolStripMenuItem.Size = new System.Drawing.Size(44, 20);
            this.hilfeToolStripMenuItem.Text = "Hilfe";
            // 
            // überDiesesToolToolStripMenuItem
            // 
            this.überDiesesToolToolStripMenuItem.Name = "überDiesesToolToolStripMenuItem";
            this.überDiesesToolToolStripMenuItem.Size = new System.Drawing.Size(160, 22);
            this.überDiesesToolToolStripMenuItem.Text = "Über dieses Tool";
            this.überDiesesToolToolStripMenuItem.Click += new System.EventHandler(this.überDiesesToolToolStripMenuItem_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 34);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(29, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "Von:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(12, 65);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(36, 13);
            this.label2.TabIndex = 2;
            this.label2.Text = "Nach:";
            // 
            // FromTextBox
            // 
            this.FromTextBox.Location = new System.Drawing.Point(53, 31);
            this.FromTextBox.Name = "FromTextBox";
            this.FromTextBox.Size = new System.Drawing.Size(459, 20);
            this.FromTextBox.TabIndex = 3;
            // 
            // ToTextBox
            // 
            this.ToTextBox.Location = new System.Drawing.Point(53, 62);
            this.ToTextBox.Name = "ToTextBox";
            this.ToTextBox.Size = new System.Drawing.Size(459, 20);
            this.ToTextBox.TabIndex = 4;
            // 
            // FromButton
            // 
            this.FromButton.Location = new System.Drawing.Point(518, 31);
            this.FromButton.Name = "FromButton";
            this.FromButton.Size = new System.Drawing.Size(61, 23);
            this.FromButton.TabIndex = 5;
            this.FromButton.Text = "...";
            this.FromButton.UseVisualStyleBackColor = true;
            this.FromButton.Click += new System.EventHandler(this.FromButton_Click);
            // 
            // ToButton
            // 
            this.ToButton.Location = new System.Drawing.Point(518, 57);
            this.ToButton.Name = "ToButton";
            this.ToButton.Size = new System.Drawing.Size(61, 23);
            this.ToButton.TabIndex = 6;
            this.ToButton.Text = "...";
            this.ToButton.UseVisualStyleBackColor = true;
            this.ToButton.Click += new System.EventHandler(this.ToButton_Click);
            // 
            // TimeRadioButton
            // 
            this.TimeRadioButton.AutoSize = true;
            this.TimeRadioButton.Location = new System.Drawing.Point(6, 19);
            this.TimeRadioButton.Name = "TimeRadioButton";
            this.TimeRadioButton.Size = new System.Drawing.Size(122, 17);
            this.TimeRadioButton.TabIndex = 7;
            this.TimeRadioButton.TabStop = true;
            this.TimeRadioButton.Text = "Konstantes kopieren";
            this.TimeRadioButton.UseVisualStyleBackColor = true;
            this.TimeRadioButton.CheckedChanged += new System.EventHandler(this.TimeRadioButton_CheckedChanged);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.ManuellRadioButton);
            this.groupBox1.Controls.Add(this.CopyButton);
            this.groupBox1.Controls.Add(this.OnChangeRadioButton);
            this.groupBox1.Controls.Add(this.TimeRadioButton);
            this.groupBox1.Location = new System.Drawing.Point(15, 88);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(564, 88);
            this.groupBox1.TabIndex = 8;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Einstellungen";
            // 
            // ManuellRadioButton
            // 
            this.ManuellRadioButton.AutoSize = true;
            this.ManuellRadioButton.Location = new System.Drawing.Point(6, 65);
            this.ManuellRadioButton.Name = "ManuellRadioButton";
            this.ManuellRadioButton.Size = new System.Drawing.Size(117, 17);
            this.ManuellRadioButton.TabIndex = 10;
            this.ManuellRadioButton.TabStop = true;
            this.ManuellRadioButton.Text = "Manuelles kopieren";
            this.ManuellRadioButton.UseVisualStyleBackColor = true;
            this.ManuellRadioButton.CheckedChanged += new System.EventHandler(this.ManuellRadioButton_CheckedChanged);
            // 
            // CopyButton
            // 
            this.CopyButton.Enabled = false;
            this.CopyButton.Location = new System.Drawing.Point(129, 63);
            this.CopyButton.Name = "CopyButton";
            this.CopyButton.Size = new System.Drawing.Size(68, 20);
            this.CopyButton.TabIndex = 9;
            this.CopyButton.Text = "Kopieren";
            this.CopyButton.UseVisualStyleBackColor = true;
            this.CopyButton.Click += new System.EventHandler(this.CopyButton_Click);
            // 
            // OnChangeRadioButton
            // 
            this.OnChangeRadioButton.AutoSize = true;
            this.OnChangeRadioButton.Location = new System.Drawing.Point(6, 42);
            this.OnChangeRadioButton.Name = "OnChangeRadioButton";
            this.OnChangeRadioButton.Size = new System.Drawing.Size(125, 17);
            this.OnChangeRadioButton.TabIndex = 8;
            this.OnChangeRadioButton.TabStop = true;
            this.OnChangeRadioButton.Text = "Intelligentes kopieren";
            this.OnChangeRadioButton.UseVisualStyleBackColor = true;
            this.OnChangeRadioButton.CheckedChanged += new System.EventHandler(this.OnChangeRadioButton_CheckedChanged);
            // 
            // timer1
            // 
            this.timer1.Interval = 500;
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // statusStrip1
            // 
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripStatusLabel1,
            this.ModusToolStripStatusLabel,
            this.toolStripStatusLabel3,
            this.toolStripStatusLabel4});
            this.statusStrip1.Location = new System.Drawing.Point(0, 181);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Size = new System.Drawing.Size(591, 22);
            this.statusStrip1.TabIndex = 9;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // toolStripStatusLabel1
            // 
            this.toolStripStatusLabel1.Name = "toolStripStatusLabel1";
            this.toolStripStatusLabel1.Size = new System.Drawing.Size(100, 17);
            this.toolStripStatusLabel1.Text = "Aktueller Modus: ";
            // 
            // ModusToolStripStatusLabel
            // 
            this.ModusToolStripStatusLabel.Name = "ModusToolStripStatusLabel";
            this.ModusToolStripStatusLabel.Size = new System.Drawing.Size(22, 17);
            this.ModusToolStripStatusLabel.Text = "---";
            // 
            // toolStripStatusLabel3
            // 
            this.toolStripStatusLabel3.Name = "toolStripStatusLabel3";
            this.toolStripStatusLabel3.Size = new System.Drawing.Size(42, 17);
            this.toolStripStatusLabel3.Text = "Status:";
            // 
            // toolStripStatusLabel4
            // 
            this.toolStripStatusLabel4.Name = "toolStripStatusLabel4";
            this.toolStripStatusLabel4.Size = new System.Drawing.Size(22, 17);
            this.toolStripStatusLabel4.Text = "---";
            // 
            // Main
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(591, 203);
            this.Controls.Add(this.statusStrip1);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.ToButton);
            this.Controls.Add(this.FromButton);
            this.Controls.Add(this.ToTextBox);
            this.Controls.Add(this.FromTextBox);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.menuStrip1);
            this.MinimumSize = new System.Drawing.Size(607, 242);
            this.Name = "Main";
            this.ShowIcon = false;
            this.Text = "EasyCopy";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Main_FormClosing);
            this.Load += new System.EventHandler(this.Main_Load);
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem dateiToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem beendenToolStripMenuItem;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox FromTextBox;
        private System.Windows.Forms.TextBox ToTextBox;
        private System.Windows.Forms.Button FromButton;
        private System.Windows.Forms.Button ToButton;
        private System.Windows.Forms.RadioButton TimeRadioButton;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.RadioButton ManuellRadioButton;
        private System.Windows.Forms.Button CopyButton;
        private System.Windows.Forms.RadioButton OnChangeRadioButton;
        private System.Windows.Forms.Timer timer1;
        private System.Windows.Forms.ToolStripMenuItem hilfeToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem überDiesesToolToolStripMenuItem;
        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel1;
        private System.Windows.Forms.ToolStripStatusLabel ModusToolStripStatusLabel;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel3;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel4;
    }
}

