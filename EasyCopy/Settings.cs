﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EasyCopy
{
    public class Settings
    {
        public string FromPath { get; set; }
        public string ToPath { get; set; }
        public Main.CopyModes Modus { get; set;}
        public int Timer { get; set; }
        public bool AdminMsg { get; set; }
    }
}
