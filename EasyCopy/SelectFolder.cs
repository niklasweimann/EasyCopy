﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace EasyCopy
{
    class SelectFolder
    {
        public string selectFolder()
        {
            string path;
            var mFolderBrowserDialog = new FolderBrowserDialog();
            mFolderBrowserDialog.ShowNewFolderButton = false;
            mFolderBrowserDialog.ShowDialog();
            path = mFolderBrowserDialog.SelectedPath;
            mFolderBrowserDialog.Dispose();
            return path;
        }
    }
}
