﻿using System;
using System.IO;
using System.Security.Principal;
using System.Windows.Forms;
using System.Xml.Serialization;

namespace EasyCopy
{
    public partial class Main : Form
    {
        public enum CopyModes
        {
            Time,
            Change,
            Manuell
        }

        private CopyModes CopyMode { get; set; }

        private CopyModes currentCopyMode { get; set; }

        readonly FileSystemWatcher watcher = new FileSystemWatcher();
        readonly SelectFolder mSelectFolder = new SelectFolder();
        readonly CopyMethodes mCopyMethodes = new CopyMethodes();
        readonly XMLWorker mXMLWorker = new XMLWorker();

        public Main()
        {
            InitializeComponent();
        }

        private void beendenToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void TimeRadioButton_CheckedChanged(object sender, EventArgs e)
        {
            if (FromTextBox.Text != "" || ToTextBox.Text != "")
            {
                ModusToolStripStatusLabel.Text = "Konstantes kopieren";
                CopyMode = CopyModes.Time;
                RunCopy();
            }
            else
            {
                MessageBox.Show("Bitte geben Sie einen gültigen Pfad an!");
                TimeRadioButton.Checked = false;
                ModusToolStripStatusLabel.Text = "---";
            }
        }

        private void OnChangeRadioButton_CheckedChanged(object sender, EventArgs e)
        {
            if (FromTextBox.Text != "" || ToTextBox.Text != "")
            {
                ModusToolStripStatusLabel.Text = "Intelligentes kopieren";
                CopyMode = CopyModes.Change;
                RunCopy();
            }
            else
            {
                MessageBox.Show("Bitte geben Sie einen gültigen Pfad an!");
                OnChangeRadioButton.Checked = false;
                ModusToolStripStatusLabel.Text = "---";
            }
        }

        private void ManuellRadioButton_CheckedChanged(object sender, EventArgs e)
        {
            if (FromTextBox.Text != "" || ToTextBox.Text != "")
            {
                ModusToolStripStatusLabel.Text = "Manuelles kopieren";
                CopyMode = CopyModes.Manuell;
                RunCopy();
            }
            else
            {
                MessageBox.Show("Bitte geben Sie einen gültigen Pfad an!");
                ManuellRadioButton.Checked = false;
                ModusToolStripStatusLabel.Text = "---";
            }
        }

        private void CopyButton_Click(object sender, EventArgs e)
        {
            mCopyMethodes.BruteforceCopy(FromTextBox.Text, ToTextBox.Text);
        }

        private void FromButton_Click(object sender, EventArgs e)
        {
            FromTextBox.Text = mSelectFolder.selectFolder();
        }

        private void ToButton_Click(object sender, EventArgs e)
        {
            ToTextBox.Text = mSelectFolder.selectFolder();
        }

        public void RunCopy()
        {
            timer1.Stop();
            CopyButton.Enabled = false;
            toolStripStatusLabel4.Text = "---";

            switch (CopyMode)
            {
                case CopyModes.Change:
                    mCopyMethodes.BruteforceCopy(FromTextBox.Text, ToTextBox.Text);
                    intelligentCopy();
                    break;
                case CopyModes.Time:
                    timer1.Start();
                    break;
                case CopyModes.Manuell:
                    CopyButton.Enabled = true;
                    break;
            }
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            toolStripStatusLabel4.Text = "Kopieren wird vorbereitet";
            if (!mCopyMethodes.BruteforceCopy(FromTextBox.Text, ToTextBox.Text))
            {
                TimeRadioButton.Checked = false;
                ManuellRadioButton.Checked = false;
                OnChangeRadioButton.Checked = false;
                timer1.Stop();
                MessageBox.Show("Beim Kopieren ist ein Fehler aufgetreten.");
                toolStripStatusLabel4.Text = "Fehler";
            }
            else
            {
                toolStripStatusLabel4.Text = "Vorgang abgeschlossen";
            }
        }

        private void intelligentCopy()
        {
            watcher.Path = FromTextBox.Text + @"\";
            watcher.Changed += new FileSystemEventHandler(OnChanged);
            watcher.EnableRaisingEvents = true;
        }

        private void OnChanged(object source, FileSystemEventArgs e)
        {
            toolStripStatusLabel4.Text = "Kopieren wird vorbereitet";
            var oldFile = new FileInfo(Path.Combine(FromTextBox.Text + @"\", Path.GetFileName(Path.GetFileName(e.FullPath))));
            var newFile = new FileInfo(Path.Combine(ToTextBox.Text, Path.GetFileName(Path.GetFileName(e.FullPath))));
            if (newFile.Exists && oldFile.Exists)
            {
                if (oldFile.LastWriteTime > newFile.LastWriteTime)
                {
                    toolStripStatusLabel4.Text = "Kopiere Daten";
                    File.Copy(Path.Combine(FromTextBox.Text + @"\", Path.GetFileName(Path.GetFileName(e.FullPath))),
                        Path.Combine(ToTextBox.Text, Path.GetFileName(Path.GetFileName(e.FullPath))),
                        true);
                }
            }
            else
            {
                File.Copy(Path.Combine(FromTextBox.Text + @"\", Path.GetFileName(Path.GetFileName(e.FullPath))),
                        Path.Combine(ToTextBox.Text, Path.GetFileName(Path.GetFileName(e.FullPath))),
                        true);
            }
            toolStripStatusLabel4.Text = "Vorgang abgeschlossen";
        }

        private void überDiesesToolToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var mAboutBox1 = new AboutBox1();
            mAboutBox1.ShowDialog();
            mAboutBox1.Dispose();
        }

        private void Main_Load(object sender, EventArgs e)
        {
            var mSettings = XMLWorker.ReadFromXML();
            if (mSettings != null)
            {
                if (mSettings.AdminMsg)
                {
                    //Überprüfung auf Adminrechte
                    var identity = WindowsIdentity.GetCurrent();
                    var principal = new WindowsPrincipal(identity);
                    if (!principal.IsInRole(WindowsBuiltInRole.Administrator))
                    {
                        MessageBox.Show("Die Anwendung wurde ohne Administratorenrechte gestartet. \n Dies kann zu unerwarteten Fehlern führen",
                            "Zu wenig Rechte",
                            MessageBoxButtons.OK,
                            MessageBoxIcon.Information);
                    }
                    else
                    {
                        this.Text += " - [Administrator]";
                    }
                }
            }
        }

        private void Main_FormClosing(object sender, FormClosingEventArgs e)
        {
            // Create Objektes with Data
            var mSettings = new Settings
            {
                AdminMsg = false,
                FromPath = FromTextBox.Text,
                Modus = CopyMode,
                Timer = 500,
                ToPath = ToTextBox.Text
            };
            XMLWorker.SaveToXML(mSettings);
        }
    }
}
