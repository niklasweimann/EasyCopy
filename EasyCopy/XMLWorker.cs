﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Principal;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml.Serialization;

namespace EasyCopy
{
    class XMLWorker
    {
        public static void SaveToXML(Settings mSettings)
        {
            try
            {
                // Create a new XmlSerializer instance with the type of the test class
                var SerializerObj = new XmlSerializer(typeof(Settings));

                // Create a new file stream to write the serialized object to a file
                TextWriter WriteFileStream = new StreamWriter(@"EasyCopy_Settings.xml");
                SerializerObj.Serialize(WriteFileStream, mSettings);

                // Cleanup
                WriteFileStream.Close();
            }
            catch (Exception e)
            {
                // Catch Exceptions
                MessageBox.Show(e.Message.ToString());
            }
        }

        public static Settings ReadFromXML()
        {
            try
            {
                //Settings einlesen
                // Create a new XmlSerializer instance with the type of the test class
                var SerializerObj = new XmlSerializer(typeof(Settings));
                // Create a new file stream for reading the XML file
                var ReadFileStream = new FileStream(@"EasyCopy_Settings.xml", FileMode.Open, FileAccess.Read, FileShare.Read);

                // Load the object saved above by using the Deserialize function
                var LoadedObj = (Settings)SerializerObj.Deserialize(ReadFileStream);

                // Cleanup
                ReadFileStream.Close();

                return LoadedObj;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message.ToString());
                return null;
            }
        }
    }
}
